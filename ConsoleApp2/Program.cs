﻿using AngleSharp.Dom.Html;
using AngleSharp.Parser.Html;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    // hey let me have a go to help you too
    //i'm trying to lookup the location and quantity of all items in a city
    //Then according to whatever item+quantity is needed, we are going to somewhere, make a purchase -> and perform further item manipulation.

    // stop what data do you have
    // i mean, what is the hierarchy - is it street which has many shop, which has many products?
    // and what do you need iequatable for again
    // sorting? first get the data organised
    // but i am right about street -> shops -> items?
    //data can be found on line 184 for steets, 215 - shop name 226 - products
    //I was thinking of having everything sorted by "item name"
    //and within item name it has the quantity cost and location, street+shop name.
    //The auctions will give us information, we need x amount of items,
    // auction? i thought it was shops
    //can i share screen for a moment it will all make sense
    //actually just go to miniconomy.com and login as aquafix, 15487953, and then on the left hand side bar, there should be something called "Auction"
    //This is an 'auction bot'                               //nice pass ;) old password yes
    //productdata, game items we can buy.
    //That is to allow for storage of 9 webpages(potential streets) i pick 9 so it's never maxed. i could find documentation how how to declare new array on variable length. 
    //You have to be located on the street to access shops on the street--

    // cc so really you need a spider and you need to build a graph so you can know how to traverse the streets? how do you travel to an unlinked street
    //no that is not the case,, yuou can travel to unlinked streets. all streets are listed on the street info see line 184so i just grab streets off default street.php, interate through default first, and furthermore.
    // kk i see how it works now
    //I didn't know an easier way to do that lol.
    //I guess you are pretending to do work or something lol

    public class WarehouseItem
    {
            public string ItemId { get; internal set; }
        public string Name { get; internal set; }
        public string Quantity { get; internal set; }
      //  public string Price { get; internal set; }

      //  public override string ToString() => $"Item for Sale: {Name} at {WarehouseItem.Name}";
    }

    public class Warehouse
    {
        public List<WarehouseItem> WarehouseItems { get; set; } = new List<WarehouseItem>();
        public float Freespace { get; internal set; }


    }
    public class Street
    {
        public List<Shop> Shops { get; set; } = new List<Shop>();
        public string Name { get; internal set; }

        public override string ToString() => $"Street: {Name}";
    }

    public class Shop
    {
        public Street Street { get; internal set; }

		public List<ItemForSale> ItemsForSale { get; set; }
        public string Name { get; internal set; }

        public override string ToString() => $"Shop: {Name}";
    }

    public class ItemForSale
    {
        public Shop Shop { get; internal set; }

        public string ItemId { get; internal set; }
        public string Name { get; internal set; }
        public string Quantity { get; internal set; }
        public string Price { get; internal set; }

        public override string ToString() => $"Item for Sale: {Name} at {Shop.Name}";
    }


    //List<GameItem> gameItem = new List<GameItem>();

    public static class miniclient
    {

        //Making property of HttpClient
        private static HttpClient _client;

        public static HttpClient Client
        {
            get { return _client; }
            set { _client = value; }
        }



        //method to download string from page
        public static async Task<string> LoadPageAsync(string p)
        {
            if (Client == null)// that means we need to login to page
            {
                Client = await Login(Client);
            }
            
            return await Client.GetStringAsync(p);

        }
        // method for logging in
        public static async Task<HttpClient> Login(HttpClient client)
        {
            client = new HttpClient();

            var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("username", "greg"),
                    new KeyValuePair<string, string>("password", "aqifix12")
                });

            var response = await client.PostAsync("https://www.miniconomy.com/login.php", content);
            return client;
        }
    }

    class Program
    {
        

       //   System.Net.CookieAwareWebClent client = new System.Net.CookieAwareWebClient();
       //Main core needs to visit the auction page first, and grab what item we are looking for.
       //Then Auction handler comes in, until new item, with boolean switch.
        public static void Main()
        {
            // MainCore().GetAwaiter().GetResult();
            //AuctionHandler().GetAwaiter().GetResult();
            //var warehousetest = await WarehouseItems();
            WarehouseCORE().GetAwaiter().GetResult();
//MainCore("shovel").GetAwaiter().GetResult();
            Console.ReadLine();
        }

        public static async Task AuctionHandler()
        {
            for (int i = 1; i > 0;)//boolean logic here!!
            {
                await Task.Delay(800);
                var data = await miniclient.LoadPageAsync("https://www.miniconomy.com/auction.php");
                System.Console.WriteLine("hihi   ==" + data);
            }
                //visit auction
                //find items
                //finish auction
                
            
        //    System.Console.WriteLine("hihi   ==" + data);

        }


        public static async Task<string> AuctionInitial()
        {
            var auctionIntial = await miniclient.LoadPageAsync("https://www.miniconomy.com/auction.php");
            var parser = new HtmlParser();

            var document1 = parser.Parse(auctionIntial);
            var item = document1.QuerySelectorAll("*[span id='prodname']");
            // document.QuerySelectorAll("*[span id='prodname']");

            System.Console.WriteLine(item.ToString());
            return null;


        }
        public static async Task WarehouseCORE()
        {
            var warehousetest = await WarehouseItems();
        }

            public static async Task MainCore(string Item)
        {
 
            var streetsData = await sShopsItems();


            var allItemsForSale = streetsData.SelectMany(street => street.Shops.SelectMany(shop => shop.ItemsForSale));

            var itemsGroupedByName = from item in allItemsForSale
                                     group item by item.Name
                                     into grp
                                     select new
                                     {
                                         Name = grp.Key,
                                         Count = grp.Count(),
                                     };           
            var mostSoldPlacesItemName = itemsGroupedByName
                .OrderByDescending(grp => grp.Count)
                .FirstOrDefault()?.Name;

            var cheapestBricksForSale = allItemsForSale
                .Where(item => item.Name == Item)
                .OrderBy(item => item.Price)
                .First();

            var shopWithCheapestBricks = cheapestBricksForSale.Shop;
            var streetWithCheapestBricks = shopWithCheapestBricks.Street;

            Console.WriteLine("Cheapest: " + cheapestBricksForSale);
        }
        /*public static void CheckShop(string shop)
        {
            shop = shop.Trim();
            shop = shop.Replace(' ', '+');
            Console.WriteLine(shop);
            string data = await miniclient.Client.GetStringAsync("https://www.miniconomy.com/shop.php?winkel=" + shop);
            var rx = new System.Text.RegularExpressions.Regex("shop_product unselectable");
            //<div class="shop_product unselectable" id="sh-glas">
            var array = rx.Split(data);
            int index = array.Length;
            for (int i = 1; i < index; i++)
            {
                string startText = "id=";//+4
                var start = array[i].IndexOf(startText);
                Console.WriteLine(start);
                var end = array[i].IndexOf(">"); // -1

                var length = (end - 1) - (start + 4);                             //Implement offsets.
                Console.WriteLine("=========entry = " + start);
                Console.WriteLine(array[i].Substring(start + 4, length));     //^^^^^^^^^^^^^^^^^
                                                                              //  Console.WriteLine(array[i]);

                //max =
                string maxstartText = "max="; //+5
                var maxstart = array[i].IndexOf("number dropshadow");//+18
                var maxend = array[i].IndexOf("</span>"); // -3
                var maxlength = (maxend) - (maxstart + 19);


                var itemKey = array[i].Substring(start + 4, length);
                var itemStock = array[i].Substring(maxstart + 19, maxlength);

                Console.WriteLine("{0,30}{1,10}", "Item", "Stock");
                if (miniclient.ItemNames.TryGetValue(itemKey, out var itemName))
                {
                    Console.WriteLine($"{itemName,30}{itemStock,10}");
                }
                else
                {
                    Console.WriteLine($"Unknown item with key {itemKey}");
                }

                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();


                /*string ItemName;
                switch(array[i].Substring(maxstart + 19, maxlength))
                {
                    case "sh-schi":
                        ItemName = "Bulletproof Vest";
                        break;
                    default:
                      //  ItemName = 
                        break;
                }
                Console.WriteLine("ItemName = " + ItemName);
                Console.WriteLine("Item Stock = " + array[i].Substring(maxstart+19, maxlength));     //^^^^^^^^^^^^^^^^^
                //title =
				
            }
        }*/


        public static async Task<List<Warehouse>> WarehouseItems()
        {
            var data = await miniclient.LoadPageAsync("https://www.miniconomy.com/mag.php");

            var WarehouseItems = new List<Warehouse>();

            var parser = new HtmlParser();

            var document1 = parser.Parse(data);
            var shopProducts = document1.QuerySelectorAll("td.center");
            Console.WriteLine(shopProducts.Count());
            //shopProducts[n]
            // shopProducts.Length
            for (int i = 3; i <= shopProducts.Count(); i++)
            {
                //shopProducts[i]
                //td.center .sprite-product
                var product = shopProducts[i].QuerySelector(".sprite-product"); //amount
                var productTitle = product.GetAttribute("title"); //Name of product in warehouse

                var productS = document1.QuerySelector("tr:nth-child(10) > .right:nth-child(2)").TextContent.Trim();
               // var productQuantity = productS.GetAttribute(".max");
        

                //var productQuantity = shopProduct.QuerySelector(".max").TextContent.Trim();
                //var productPrice = shopProduct.QuerySelector(".shop_product_price").TextContent.Trim();
                Console.WriteLine("productID = " + i);
                Console.WriteLine("---------------");
                Console.WriteLine("Amount of product = " + product);
                Console.WriteLine("---------------");
                Console.WriteLine("Name of product = " + productTitle);
                Console.WriteLine("---------------");
                Console.WriteLine("Amount of product = " + productS);

                Console.WriteLine(i);
            }

           /* foreach (var shopProduct in shopProducts)
                        {
      

                int productId = Int32.Parse(shopProduct.Id); //passes null
                if (productId > 2)
                {

                    
                }
                //          Console.WriteLine($"{productTitle,-50}{productQuantity,10}{productPrice,20}");
   //             /*
                var itemsForSale = new List<WarehouseItem>();
                itemsForSale.Add(new WarehouseItem
                            {
								Name = productTitle,
                                ItemId = productId,
								Quantity = productQuantity,
								//Price = productPrice,
                           });
            } 
        */

            //end
            return WarehouseItems;
        }







        public static async Task<List<Street>> sShopsItems()
        {
            //everything back to normal?
            // what have you done you've ruined the entire app lol
            var data = await miniclient.LoadPageAsync("https://www.miniconomy.com/street.php");
            //t.GetStringAsync("https://www.miniconomy.com/street.php");

            var parser = new HtmlParser();

			var document1 = parser.Parse(data);
            var streets = document1.QuerySelectorAll("a.street-polesign.street-polesign-streetname");

            var streetsData = new List<Street>();

            foreach (var street in streets)
            {
                var streetHref = street.GetAttribute("href");

                // todo: cbf finding the street name atm
                var streetData = new Street();
                streetsData.Add(streetData);
                streetData.Name = streetHref;

                data = await miniclient.Client.GetStringAsync("https://www.miniconomy.com/" + streetHref);
                var streetDocument = parser.Parse(data);

                var shopCells = streetDocument.QuerySelectorAll("#street_table td");

                var requests = shopCells
                    .Select(
                        shopCell =>
                        {
                            var shopNameBase = shopCell.QuerySelector("b")?.TextContent?.Trim();
                            var shopNameTt = shopCell.QuerySelector("b > tt")?.TextContent?.Trim();
                            var shopName = shopNameTt ?? shopNameBase;

                            var shopQty = shopCell.QuerySelector("i")?.TextContent?.Trim();

                            if (string.IsNullOrWhiteSpace(shopName) || string.IsNullOrWhiteSpace(shopQty))
                            {
                                return null;
                            }

                            var url = "https://www.miniconomy.com/shop.php?winkel=" + shopName;

                            return url;
                        })
                    .Where(url => url != null)
                    .Select(url => new { url, data = miniclient.Client.GetStringAsync(url) });
                
                foreach (var shopCell in shopCells) //Visiting the shop!!
                {
                    try
                    {
                        var shopNameBase = shopCell.QuerySelector("b")?.TextContent?.Trim();
                        var shopNameTt = shopCell.QuerySelector("b > tt")?.TextContent?.Trim();
                        var shopName = shopNameTt ?? shopNameBase;

                        var shopQty = shopCell.QuerySelector("i")?.TextContent?.Trim();

                        if (string.IsNullOrWhiteSpace(shopName) || string.IsNullOrWhiteSpace(shopQty))
                        {
                            continue;
                        }

                        var url = "https://www.miniconomy.com/shop.php?winkel=" + shopName;
                        var shopData = await requests.First(r => r.url == url).data;

                        Console.WriteLine("{0,-60}", "Shop Name");
                        Console.WriteLine($"{shopName,-60}");
                        Console.WriteLine();

                        var shopDocument = parser.Parse(shopData);

                        var shopProducts = shopDocument.QuerySelectorAll(".shop_product");

                        Console.WriteLine("{0,-50}{1,10}{2,20}", "Product", "Quantity", "Price");

                        var itemsForSale = new List<ItemForSale>();

                        foreach (var shopProduct in shopProducts)
                        {
                            var productId = shopProduct.Id;
                            var product = shopProduct.QuerySelector(".sprite-product");
                            var productTitle = product.GetAttribute("title");

                            var productQuantity = shopProduct.QuerySelector(".shop_product_number").TextContent.Trim();
                            var productPrice = shopProduct.QuerySelector(".shop_product_price").TextContent.Trim();

                            Console.WriteLine($"{productTitle,-50}{productQuantity,10}{productPrice,20}");

                            itemsForSale.Add(new ItemForSale
                            {
								Name = productTitle,
                                ItemId = productId,
								Quantity = productQuantity,
								Price = productPrice,
                            });
                        }

                        var newShop = new Shop
                        {
                            Street = streetData,
                            Name = shopName,
                            ItemsForSale = itemsForSale,
                        };
                        newShop.ItemsForSale.ForEach(item => item.Shop = newShop);
                        streetData.Shops.Add(newShop);

                        Console.WriteLine();
                        Console.WriteLine();
                    }
                    catch (Exception)
                    {
                        // If there's an error, ignore. The winkel might not be for a shop - might be a club etc.
                    }
                }
            }


            // work with streetsdata here
        
              return streetsData;

            // old blake code 
            #region OldBlake Code
            /*
            // why are we using an array? why not a loop? what are you trying to achieve?
            //I need this data to be accessible.
            //i'm trying to lookup the location and quantity of all items in a city
            //Then according to whatever item+quantity is needed, we are going to somewhere, make a purchase -> and perform further item manipulation.

            //info need to be stored like this?
            //      {product, price, quantity, street, store }
            //       String, double, int, String, String
            IHtmlDocument[] document = new IHtmlDocument[9];        //#street holder
                document[0] = parser.Parse(data);
            var streetList = document[0].QuerySelector(".street-polesigns");

            var streetsTovisit = streetList.QuerySelectorAll("a.street-polesign.street-polesign-streetname");

			
            var sI = 0;             //Street Index
            //street Index, document -> document[9] #see line 119
                //First street, default
                    //second street data sI = 1
                    //third stree data sI = 2, etc..
            foreach (var street in streetsTovisit)
            {
                try
                {                                                          //Find Streets
                    var href = street.GetAttribute("href");             // this is "street.php?=blake+street" or something very similar.
                    streetList = document[sI].QuerySelector(".street-polesigns");

                    streetsTovisit = streetList.QuerySelectorAll("a.street-polesign.street-polesign-streetname");
                    sI++;
                    data = await miniclient.Client.GetStringAsync("https://www.miniconomy.com/" + href);
                    document[sI] = parser.Parse(data);
                    var isss = document.Length;


                  
                        var streetTable = document[sI].QuerySelector("#street_table");
                        var shopCells = streetTable.QuerySelectorAll("td");
                        foreach (var shopCell in shopCells)
                        {
                            try
                            {
                                var shopNameBase = shopCell.QuerySelector("b")?.TextContent?.Trim();
                                var shopNameTt = shopCell.QuerySelector("b > tt")?.TextContent?.Trim();
                                var shopName = shopNameTt ?? shopNameBase;

                                var shopQty = shopCell.QuerySelector("i")?.TextContent?.Trim();

                                if (string.IsNullOrWhiteSpace(shopName) || string.IsNullOrWhiteSpace(shopQty))
                                {
                                    continue;
                                }

                                var shopData = await miniclient.Client.GetStringAsync("https://www.miniconomy.com/shop.php?winkel=" + shopName);

                                Console.WriteLine("{0,-60}", "Shop Name");
                                Console.WriteLine($"{shopName,-60}");
                                Console.WriteLine();

                                var shopDocument = parser.Parse(shopData);

                                var shopProducts = shopDocument.QuerySelectorAll(".shop_product");

                                Console.WriteLine("{0,-50}{1,10}{2,20}", "Product", "Quantity", "Price");

                                foreach (var shopProduct in shopProducts)
                                {
                                    var product = shopProduct.QuerySelector(".sprite-product");
                                    var productTitle = product.GetAttribute("title");

                                    var productQuantity = shopProduct.QuerySelector(".shop_product_number").TextContent.Trim();
                                    var productPrice = shopProduct.QuerySelector(".shop_product_price").TextContent.Trim();

                                    Console.WriteLine($"{productTitle,-50}{productQuantity,10}{productPrice,20}");
                                }

                                Console.WriteLine();
                                Console.WriteLine();
                            }
                            catch (Exception)
                            {
                                // If there's an error, ignore. The winkel might not be for a shop - might be a club etc.
                            }
                        }

                    

                    Console.WriteLine("====== " + href);
                    
                }
                catch (Exception)
                {
                    // If there's an error, ignore. The winkel might not be for a shop - might be a club etc.
                }
            }


            // what is document[0]?
       


            Console.WriteLine("Press any key to keep going...");
            Console.ReadKey();
            */
            #endregion
        }
        /*
        public static void CheckStreet()
        {
            string data = miniclient.Client.DownloadString("https://www.miniconomy.com/street.php");




            //regex <td><a
            var rx = new System.Text.RegularExpressions.Regex("<td><a");
            var array = rx.Split(data);
            int index = array.Length;
            for (int i = 1; i + 1 < index; i++)
            {
                const string startText = "<b>";//+3
                var start = array[i].IndexOf(startText);
                Console.WriteLine(start);
                var end = array[i].IndexOf("</b>"); // 4

                var length = (end) - (start + 3);                             //Implement offsets.
                Console.WriteLine(array[i].Substring(start + 3, length));     //^^^^^^^^^^^^^^^^^
                CheckShop(array[i].Substring(start + 3, length));
                //  Console.WriteLine(array[i]);
            }
        }
        public static void CheckWH()
        {

            var rx = new System.Text.RegularExpressions.Regex("<td align=" + '"' + "center" + '"' + "><div class=");
            string split = "<td align=" + '"' + "center" + '"' + "><div class=";
            string splitend = "<td alight=" + '"' + "center" + '"' + "><input type =";

            //  @"<td align =""center""><div class=".ToString();
            string data = miniclient.Client.DownloadString("https://www.miniconomy.com/mag.php");

            var array = rx.Split(data);

            string[] DataEnd = data.Split(split.ToCharArray());

            //<td align="center"><input type="number" 

            int index = array.Length;
            string[] s = new string[index];

            for (int i = 1; i < index; i++)
            {
                //Now have each warehouse item, time to extract name and quantity, and serialize.

                // s[i] = System.Text.RegularExpressions.Regex.Match(array[i], @"title=""""""(.+)""""""></div></td>",System.Text.RegularExpressions.RegexOptions.Singleline).Groups[1].Value;
                const string startText = "title=";// +1 to ignore quotes
                var start = array[i].IndexOf(startText) + startText.Length;
                var end = array[i].IndexOf("></div></td>"); //-1 ^

                var length = (end - 2) - (start);                             //Implement offsets.
                Console.WriteLine(array[i].Substring(start + 1, length));     //^^^^^^^^^^^^^^^^^
                var itemName = array[i].Substring(start + 1, length);

                const string startTextCount = "<td align=";// +9
                var startCount = array[i].IndexOf(startTextCount);
                var endCount = array[i].IndexOf("</td>", array[i].IndexOf("</td>") + 5); //

                //int index = s.IndexOf(',', s.IndexOf(',') + 1);

                var lengthCount = (endCount - 19) - (startCount);
                // Console.WriteLine("endcount = " + endCount + " And startcount = " + startCount);
                Console.WriteLine(array[i].Substring(startCount + 19, lengthCount));     //^^^^^^^^^^^^^^^^^
                var itemCount = array[i].Substring(startCount + 19, lengthCount);
                //  <td align="center">3</td>
                //Print split lines...
                //Console.WriteLine(s[i]);
                miniclient.WHstock.Add(itemName, Int32.Parse(itemCount));
            }
            //  miniclient.WHstock


            //string DataEnd = data.Split(@"< td align = ""center"" >< div class=""");
            //Console.WriteLine(miniclient.Client.DownloadString("https://www.miniconomy.com/mag.php"));

        }
        public void Login(string user, string pass)
        {
        }
        */
    }
}
namespace System.Net
{
    using System.Collections.Specialized;
    using System.Linq;
    using System.Text;

    public class CookieAwareHttpClient : HttpClient
    {


        public void Login(string loginPageAddress, NameValueCollection loginData)
        {
            CookieContainer container;

            var request = (HttpWebRequest)WebRequest.Create(loginPageAddress);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36";
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            request.Referer = "https://www.miniconomy.com/";

            var query = string.Join("&",
              loginData.Cast<string>().Select(key => $"{key}={loginData[key]}"));

            var buffer = Encoding.UTF8.GetBytes(query);
            //Encoding.ASCII.GetBytes(query);
            request.ContentLength = buffer.Length;
            var requestStream = request.GetRequestStream();
            requestStream.Write(buffer, 0, buffer.Length);
            requestStream.Close();

            container = request.CookieContainer = new CookieContainer();

            var response = request.GetResponse();
            response.Close();
            CookieContainer = container;
        }

        public CookieAwareHttpClient(CookieContainer container)
        {
            CookieContainer = container;
        }

        public CookieAwareHttpClient()
          : this(new CookieContainer())
        { }

        public CookieContainer CookieContainer { get; private set; }


    }
}